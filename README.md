Welcome to my secret garden. Here you'll find wonders and miracles. [insert rainbow emoji here]

- Code Snippets : various random pieces of code that may be useful one day
- Lua : lua scripts, custom actions and Lua Sender layouts
- Powershell : useful Powershell scripts or code snippets. WindowsPowerShell subfolder contains the definition of several powershell functions I use every day to improve my productivity.
- Projects : some scripts used on client projects.
- Survival Kits : some notes and tips on various subjects (network, config, hardware, command line, XA, CC...) that saved / will save my life one day.
- TestRail : backup of some old TestRail scripts.
- Training Notes : step-by-step wefts of several trainings I've been giving. Useful to prepare them, evaluate the timing and remember everything to say.
