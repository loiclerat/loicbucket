ID3D11InfoQueue* mInfoQueue;
+	HRESULT hr = device->QueryInterface(__uuidof(ID3D11InfoQueue), (LPVOID*)&mInfoQueue);
+	if (SUCCEEDED(hr))
+	{
+		// For some reasons this prevents null messages causing crash when Ogre tries to read them later
+		mInfoQueue->PushEmptyRetrievalFilter();
+	}