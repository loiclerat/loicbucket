$nasHostname = "LCL-01-NAS-01"
$backupHostname = "LCL-01-XAG02"
$projectName = "LCL"

$watcher = New-Object System.IO.FileSystemWatcher
$watcher.IncludeSubdirectories = $true

$watcher.Path = 'D:\test'
$watcher.EnableRaisingEvents = $true

$action =
{
    $path = $event.SourceEventArgs.FullPath
    $changetype = $event.SourceEventArgs.ChangeType
    Write-Host "Hello something changed"
    Write-Host "$path was $changetype at $(get-date)"
}

Register-ObjectEvent $watcher 'Created' -Action $action