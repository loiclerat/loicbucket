info = generateBehaviourInfo("MyAction", "Faire un play timeline custom", "My Action (displayName)")

pt_timelineName = addParameter(info, 
								genParamHeader("Timeline to play (internal name)", "Timeline to play yo (display name)", "Timeline to play ya (description)", "checkList"),
								genParamValues("", {"%TIMELINES%"}))

function MyAction_init()
	local objects = splitString( getParameterValue(pt_timelineName) , ',' )
	for k,timelineIDStr in pairs(objects) do

		local timelineID = tonumber(timelineIDStr) -- convert the argument to a number
		local timelineName = Integration:getTimeline(timelineID):getName()	
		local timelinesPlaying = xf_timelinemgr:getPlayingTimelines()
		local timelineIsPlaying = false
		for word in string.gmatch(timelinesPlaying, '([^,]+)') do
			if(word == timelineName) then
				timelineIsPlaying = true
			end
		end
		
		if (timelineIsPlaying == false) then
			xf_timelinemgr:startTimeline(timelineID)
		end
	end
end