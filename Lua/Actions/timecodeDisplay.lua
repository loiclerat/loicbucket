info = generateBehaviourInfo("timecodeDisplay", "Display Current time in a text element", "Timecode Display")

td_textElementID = 	addParameter(info, 
								 genParamHeader("td_textElementID", "Text Element ID", "ID of the Text Element", "int"),
								 genParamValues(0))
					
					
textElement = nil
timeline = nil
		
function timecodeDisplay_init()
	timeline = Integration:getTimeline(_TIMELINE_ID_)
	textElement = Integration:getTimelineElementById(getParameterValue(td_textElementID))
end

function timecodeDisplay_update(dt)
	currentTimeMs = Integration:getTimeline(_TIMELINE_ID_):getLastEvaluationTime()
	if textElement ~= nil then
		timeInt = math.floor(currentTimeMs)
		hours = math.floor((timeInt % 86400000) / 3600000);
		minutes = math.floor((timeInt % 3600000) / 60000);
		seconds = math.floor((timeInt % 60000) / 1000);
		frame = math.floor(timeInt / 1000.0 * 30) % 30
		timeStr = string.format("%02d", hours) .. ":" .. string.format("%02d", minutes) .. ":" .. string.format("%02d", seconds) .. "." .. string.format("%02d", frame)
		
		textElement:getDrawing(0):getFirstDrawableText():setText(timeStr)
	end
end

function timecodeDisplay_close()
end