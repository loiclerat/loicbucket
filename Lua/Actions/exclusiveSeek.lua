info = generateBehaviourInfo("exclusiveSeek", "Exclusive Seek", "Exclusive Seek")

es_continuePlaying = 				addParameter(info,
										genParamHeader("Keep playing", getTranslation("Keep_playing"), getTranslation("st_Keep_playing_info"), "bool"),
									genParamValues(true))

es_hourToSeek = 				addParameter(info,
										genParamHeader("Hour", getTranslation("Hour"), getTranslation("st_Hour_info"), "int"),
								genParamValues(0))

es_minuteToSeek = 				addParameter(info,
										genParamHeader("Minute", getTranslation("Minute"), getTranslation("st_Minute_info"), "int"),
								genParamValues(0, 0, 59))

es_secondToSeek = 				addParameter(info,
										genParamHeader("Second", getTranslation("Second"), getTranslation("st_Second_info"), "int"),
								genParamValues(0, 0, 59))

es_msecondToSeek = 				addParameter(info,
										genParamHeader("Millisecond", getTranslation("Millisecond"), getTranslation("st_Millisecond_info"), "int"),
								genParamValues(0, 0, 999))
								
es_filterType = 				addParameter( 	info,
								genParamHeader("Filter Types", getTranslation("filterType"), getTranslation("filterType_info"), "string", false),
								genParamValues("FT_NODE"))	

function exclusiveSeek_init()

	local msToSeek =   getParameterValue(es_msecondToSeek) 
				+ 1000.0*getParameterValue(es_secondToSeek) 
				+ 1000.0*60.0*getParameterValue(es_minuteToSeek) 
				+ 1000.0*60.0*60.0*getParameterValue(es_hourToSeek) 
				
	xf_timelinemgr:markTimelineForSeekInternal(_TIMELINE_ID_, msToSeek, getParameterValue(es_continuePlaying))

end

function exclusiveSeek_preload()
	local msToSeek =   getParameterValue(es_msecondToSeek) 
				+ 1000.0*getParameterValue(es_secondToSeek) 
				+ 1000.0*60.0*getParameterValue(es_minuteToSeek) 
				+ 1000.0*60.0*60.0*getParameterValue(es_hourToSeek) 
	xf_timelinemgr:preloadTimelineAtTime(xf_timelinemgr:getTimelineIdName(_TIMELINE_ID_), msToSeek)
end