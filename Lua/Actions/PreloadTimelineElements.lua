info = generateBehaviourInfo("PreloadTimelineElements", "Preload the specified Timeline Elements.", "Preload Timeline Elements")

pte_timelineName = addParameter(info,
				genParamHeader("timelineName", getTranslation("timelines_label"), getTranslation("pt_timelineName_info"), "checkList"),
				genParamValues("", {"%TIMELINES%"}))
				
pte_elementsIDs = addParameter(	info,
							genParamHeader("Element IDs", "Element IDs", "Comma separated list of Timeline Elements to fade.", "string", false),
							genParamValues(""))
				
function PreloadTimelineElements_init()
	local timelines = splitString( getParameterValue(pte_timelineName) , ',' )
	
	for k,timelineIDStr in pairs(timelines) do
		timelineName = Integration:getTimeline(tonumber(timelineIDStr)):getName()
		xf_timelinemgr:preloadTimelineElements(timelineName, getParameterValue(pte_elementsIDs))
	end
end