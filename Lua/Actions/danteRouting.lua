info = generateBehaviourInfo("danteRouting", "Batch Dante routing", getTranslation("Dante Routing"))

danteRouting_ip = 				addParameter( 	info,
								genParamHeader("IP", "IP", "Destination IP address", "string", false),
								genParamValues("127.0.0.1"))

danteRouting_port = 			addParameter(info,
								genParamHeader("Port", getTranslation("Port"), getTranslation("udpout_Port_info"), "int", false),
								genParamValues(4440, 1, 65535))
								
danteRouting_transmitterName = 	addParameter( 	info,
								genParamHeader("Transmitter Name", "Transmitter Name", "Transmitter Name", "string", false),
								genParamValues(""))
								
danteRouting_formatting = 		addParameter(	info,
								genParamHeader("Formatting", getTranslation("HexaFormatting"), getTranslation("HexaFormatting_info"), "stringList", false),
								genParamValues(2, {"string", "hexadecimal"}))					
								
danteRouting_nullchar = 				addParameter( 	info,
								genParamHeader("Append null character", getTranslation("Nullchar"), getTranslation("udpout_nullchar_info"), "bool", false),
								genParamValues(true))
								
danteRouting_startChannel = 	addParameter( 	info,
								genParamHeader("Start Channel", "Start Channel", "Start Channel", "int", false),
								genParamValues(1,1,128))
								
danteRouting_endChannel = 		addParameter( 	info,
								genParamHeader("End Channel", "End Channel", "End Channel", "int", false),
								genParamValues(1,1,128))


danteRouting_filterType = 			addParameter( 	info,
								genParamHeader("Filter Types", getTranslation("filterType"), getTranslation("filterType_info"), "string", false),
								genParamValues("FT_NODE"))	
								
function danteRouting_init()
	local formattingOption = getParameterValue(danteRouting_formatting)
	local hexFormat = (formattingOption == "hexadecimal")
	
	local deviceName = getParameterValue(danteRouting_transmitterName)
	local deviceNameAscii = ""
	for i=1,string.len(deviceName) do
		deviceNameAscii = deviceNameAscii .. string.format("%02x", string.byte(deviceName, i))
	end
	
	for i=getParameterValue(danteRouting_startChannel),getParameterValue(danteRouting_endChannel) do
		local message = "280801250090341000000000000000000800200100" .. string.format("%02x", i) .. "000301150118000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
		if (i > 99) then
			message = message .. string.format("31%x%x", string.byte(math.floor((i-100)/10)), string.byte(i%10))
		else
			message = message .. string.format("%x%x", string.byte(math.floor(i/10)), string.byte(i%10))
		end
		
		message = message .. "00" .. deviceNameAscii
		message = message:gsub("..", "%1 "):sub(1,-2)
		
		print(message)
		
		xf_globalmgr:sendUDPMessage( getParameterValue(danteRouting_ip), getParameterValue(danteRouting_port), message, getParameterValue(danteRouting_nullchar), hexFormat)
	end
end