- X-Agora basics (2h)
	- System overview
	- Files/folders/processes
	- Access the system
	- Designer overview
	- Mapping tools
- Project overview (1h)
	- Show timelines
	- Manual operation
	- Projection study
	- Player config
________________________________________

- Hardware/OS (1h)
	- Graphics
	- Timecode
	- Access (VNC...)
	- Syno
	- Good practices
- Troubleshooting (2h)
	- Logs
	- Project recovery
	- Processes and status
	- Player offline/not running
	- Video issues
	- Exercises ?
- Backup system (2h)
	- Scripts and scheduled tasks
	- Procedure (demo)
	- Hands-on ?
- Mapping
	- Reverse mapping
	- Live masking
	- Warping ?
	- Presets
	- Hands-on
	
___________________

= Day 1
	- Maintenance
= Day 2
	- Maintenance
= Day 3
	- XA basics (theoretical)
	- Project overview (theoretical)
	- Hardware/OS
	- Mapping
= Day 4
	- Troubleshooting
	- Mapping
= Day 5
	- Backup system
	- Mapping