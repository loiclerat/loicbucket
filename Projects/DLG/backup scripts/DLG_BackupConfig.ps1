# The only argument should be the hostname of the server to replace
if ($args.count -ne 1)
{
	'Wrong number of arguments provided'
	exit 1
}

$workingDir = Split-Path $script:MyInvocation.MyCommand.Path
$implPath = $workingDir + "\DLG_BackupConfig_impl.ps1"

if(-not(Test-Path $implPath))
{
	Write-Error "File not found : $implPath"
	exit 1
}

# The actualy script will never properly return to the Control Center because the connection will break when setting the IP address
# So we start the actual script in another process and don't wait for it to finish
Start-Process powershell -ArgumentList "-File $implPath","$args"