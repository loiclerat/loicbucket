function process($serverToReplace)
{
	"Configuring computer as $serverToReplace..."
	
	$ipSuffix = 0
	
	switch($serverToReplace)
	{
		"DLG-CTL-INT01" { $ipSuffix = 21 ; break}
		"DLG-CTL-INT02" { $ipSuffix = 22 ; break}
		"DLG-CTL-INT03" { $ipSuffix = 23 ; break}
		"DLG-CTL-INT04" { $ipSuffix = 24 ; break}
		"DLG-CTL-INT05" { $ipSuffix = 25 ; break}
		"DLG-CTL-INT06" { $ipSuffix = 26 ; break}
		"DLG-CTL-AUD01" { $ipSuffix = 31 ; break}
		"DLG-CTL-AUD02" { $ipSuffix = 32 ; break}
		"DLG-CTL-AUD03" { $ipSuffix = 33 ; break}
		default 
		{ 
			"The server hostname provided is invalid. Canceling..."
			exit
		}
	}
	

	## Set IP addresses ##

	if ($serverToReplace -like '*INT*')
	{
		# Show-Control
		netsh interface ip set address '01-S-Showcontrol' static "address=172.16.201.$ipSuffix" 'mask=255.255.0.0' 'gateway=172.16.0.1'
		"IP address configured on Show-Control (172.16.201.$ipSuffix)"
	
		# Video
		netsh interface ip set address '03-V-Video' static "address=172.20.200.$ipSuffix" 'mask=255.255.255.0'
		"IP address configured on Video (172.20.200.$ipSuffix)"
	}
	else
	{
		# Show-Control
		netsh interface ip set address '02-S-Showcontrol' static "address=172.16.210.$ipSuffix" 'mask=255.255.0.0' 'gateway=172.16.0.1'
		"IP address configured on Show-Control (172.16.210.$ipSuffix)"
	}

	## Set hostname ##

	"`nRenaming computer as $serverToReplace."
	Rename-Computer -NewName "$serverToReplace"

	"`nConfiguration complete! The computer will restart in 10 seconds"

	## Restart ##
	Start-Process -FilePath "shutdown" -Argumentlist "/r","/t 10"
}

$scriptPath = Split-Path $script:MyInvocation.MyCommand.Path
$logFilePath = "$scriptPath\log.txt"

# Flush the log file if it's too big (more than 10MB)
if((Get-Item $logFilePath).length / 1MB -gt 10)
{
	"Flushing log file..."
	Clear-Content $logFilePath
}

$date = Get-Date

"`n----`n$date - Executing Backup Config script`n" *>> $logFilePath


# Execute the script redirecting all output to the log.txt file
process $args[0] *>> $logFilePath