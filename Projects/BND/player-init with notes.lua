function initArtnetPixelMapping(LedPlaneId,ipAddress,x,y,universe, ExtraAppendName, startingIndex, FirstRGB_MiddleLineIndex, offsetYimage)

	LedPlaneName = LedPlaneId .. "__Screen__Camera__"
	uniqueName = LedPlaneId .. tostring(universe) .. ExtraAppendName
    
	local artnetSource = fixture_plugin:getFixtureMappingSource(LedPlaneId .. 'Source')
	if (artnetSource == nil) then
		logicServer("Unique fixture name created (+SourceRGB, +RGB)" .. LedPlaneId)
		artnetSource = fixture_plugin:createSourceFromCamera(LedPlaneId .. 'Source', LedPlaneName, x, y)
	end
	
	fixtureRGB = fixture_plugin:createArtnetFixture(uniqueName .. 'RGB', FixtureManager.CF_RGB, startingIndex, universe, 4*y)
	fixtureRGB:setIP(ipAddress)
	fixtureRGB:setPort(6454)
    
	fixtureW = fixture_plugin:createArtnetFixture(uniqueName ..'W', FixtureManager.CF_GREYSCALE, startingIndex, universe, 4*y)
	fixtureW:setIP(ipAddress)
	fixtureW:setPort(6454)
    
	mappingRGB = fixture_plugin:createMapping(uniqueName ..'RGB', artnetSource, fixtureRGB)
	mappingW = fixture_plugin:createMapping(uniqueName ..'W',artnetSource, fixtureW)
    
	for i=0,y-1 do
		                                  --X de l'image,		y de l'image, DMX address
		mappingRGB:addChannelMappingPoint(FirstRGB_MiddleLineIndex, i+offsetYimage+1, i*4+1)
		--1
		--5
		--9
		mappingW:addChannelMappingPoint(FirstRGB_MiddleLineIndex+10, i+offsetYimage+1, i*4+4)
		--4
		--8
		--12
	end
end


function initArtnetPixelMappingLabrosse(universe, FirstRGB_MiddleLineIndex)

	LedPlaneId="1156751"
	ipAddress='172.16.207.8'
	x=40
	y=218
	offsetYimage = 128
	secondUniversLedNumber = (y-offsetYimage)  --218 - 128
	LedPlaneName = LedPlaneId .. "__Screen__Camera__"
	uniqueName = LedPlaneId .. tostring(universe)

	local artnetSource = fixture_plugin:getFixtureMappingSource(LedPlaneId .. 'Source')
	if (artnetSource == nil) then
		logicServer("Unique fixture name created (+SourceRGB, +RGB)" .. LedPlaneId)
		artnetSource = fixture_plugin:createSourceFromCamera(LedPlaneId .. 'Source', LedPlaneName, x, y)
	end

	fixtureRGB = fixture_plugin:createArtnetFixture(uniqueName .. 'RGB', FixtureManager.CF_RGB, 1, universe, 4*offsetYimage)
	fixtureRGB2 = fixture_plugin:createArtnetFixture(uniqueName .. 'RGB2', FixtureManager.CF_RGB, 1, universe+1, 4*secondUniversLedNumber)
	fixtureRGB:setIP(ipAddress)
	fixtureRGB:setPort(6454)
	fixtureRGB2:setIP(ipAddress)
	fixtureRGB2:setPort(6454)

	fixtureW = fixture_plugin:createArtnetFixture(uniqueName..'W', FixtureManager.CF_GREYSCALE, 1, universe, 4*offsetYimage)
	fixtureW2 = fixture_plugin:createArtnetFixture(uniqueName..'W2', FixtureManager.CF_GREYSCALE, 1, universe+1, 4*secondUniversLedNumber)
	fixtureW:setIP(ipAddress)
	fixtureW:setPort(6454)
	fixtureW2:setIP(ipAddress)
	fixtureW2:setPort(6454)

	mappingRGB = fixture_plugin:createMapping(uniqueName..'RGB', artnetSource, fixtureRGB)
	mappingW = fixture_plugin:createMapping(uniqueName..'W',artnetSource, fixtureW)
	mappingRGB2 = fixture_plugin:createMapping(uniqueName..'RGB2', artnetSource, fixtureRGB2)
	mappingW2 = fixture_plugin:createMapping(uniqueName..'W2',artnetSource, fixtureW2)

	for i=0,offsetYimage-1 do
		mappingRGB:addChannelMappingPoint(FirstRGB_MiddleLineIndex, i+1, i*4+1)
		--1
		--5
		--9
		mappingW:addChannelMappingPoint(FirstRGB_MiddleLineIndex+10, i+1, i*4+4)
		--4
		--8
		--12
	end

	for i=0,secondUniversLedNumber-1 do
		mappingRGB2:addChannelMappingPoint(FirstRGB_MiddleLineIndex, i+offsetYimage+1, i*4+1)
		--1
		--5
		--9
		mappingW2:addChannelMappingPoint(FirstRGB_MiddleLineIndex+10, i+offsetYimage+1, i*4+4)
		--4
		--8
		--12
	end
end


function myStartupCallback()
	logicServer("The player has finished loading.")
	xf_globalmgr:loadPlugin('fixture_plugin')

	if (xf_globalmgr:matchUser("BND-PC-03")) then
		initArtnet()
		logicServer("Arnet mapping init")
	end

end

function initArtnet()
                             --ID      ,    IP    , fixture, uni, chanOffset, imageOffset[x,y]
	initArtnetPixelMapping("1156757", '172.16.207.1',  40, 109,  1-1, "-",   1,  5, 0) Path only artefacts W not played in show
	initArtnetPixelMapping("1156757", '172.16.207.2',  40, 109,  2-1, "-",   1,  5, 0 ) Path only artefacts W not played in show
                                                                        
	--Confessional Holograme                                            
	initArtnetPixelMapping("1156727", '172.16.207.3',  80,  54,  3-1, "A",   1,  5, 0)  Path W show no W  --Confessional Gauche Strip Gauche
	initArtnetPixelMapping("1156727", '172.16.207.3',  80,  54,  3-1, "B", 217, 25, 0)  Path W show no W --Confessional Gauche Strip Droite
	initArtnetPixelMapping("1156727", '172.16.207.3',  80,  54,  4-1, "C",   1, 45, 0)  Path W show no W --Confessional droite, Strip de Gauche
	initArtnetPixelMapping("1156727", '172.16.207.3',  80,  54,  4-1, "D", 217, 65, 0)  Path W show no W --Confessional droite, Strip de Droite
                                                                        
  --Anges                                                               
	initArtnetPixelMapping("1156752", '172.16.207.4',  40, 109,  5-1, "-",   1,  5, 0) Path and show no W
	initArtnetPixelMapping("1156752", '172.16.207.5',  40, 109,  6-1, "-",   1, 25, 0) Path and show no W
	--Enseignante                                                       
	initArtnetPixelMapping("1156756", '172.16.207.6',  40, 109,  7-1, "-",   1,  5, 0) Path and show no W
	initArtnetPixelMapping("1156756", '172.16.207.7',  40, 109,  8-1, "-",   1, 25, 0) Path and show no W
                                                                        
  --Christ De Labrosse                                                  
	--fixture1                                                          
	--image size 40*128                                                 
	--led rgbw size 40*4*128=512                                         
	initArtnetPixelMappingLabrosse(9-1, 5) Path no W, not played in show                     
	initArtnetPixelMappingLabrosse(11-1, 25) Path no W, not played in show                      
                                                                         
	--Led Margerite Bourgeois                                            
	initArtnetPixelMapping("1156749", '172.16.207.9',  40,  72, 13-1, "A-",  1,  5, 0) Not played in Path and show
	initArtnetPixelMapping("1156749", '172.16.207.9',  40,  72, 14-1, "B-",  1, 25, 0) Not played in Path and show
                                                                        
	--Confessional Audio                                                
	initArtnetPixelMapping("1156760", '172.16.207.10', 80,  54, 15-1,  "A",   1,  5, 0) Path W (uniform image) show no W
	initArtnetPixelMapping("1156760", '172.16.207.10', 80,  54, 15-1,  "B", 217, 25, 0) Path W (uniform image) show no W
	initArtnetPixelMapping("1156760", '172.16.207.10', 80,  54, 16-1,  "C",   1, 45, 0) Path W (uniform image) show no W
	initArtnetPixelMapping("1156760", '172.16.207.10', 80,  54, 16-1,  "D", 217, 65, 0) Path W (uniform image) show no W
                                                                           
	--St-Joseph Gauche                                                     
	initArtnetPixelMapping("1156753", '172.16.207.11', 40, 109, 17-1,  "-",   1,  5, 0) Path and show no W
	--St-Joseph Droite                                                     
	initArtnetPixelMapping("1156753", '172.16.207.12', 40, 109, 18-1,  "-",   1, 25, 0) Path and show no W
	--Mandian Gauche                                                      
	initArtnetPixelMapping("1156754", '172.16.207.13', 40, 109, 19-1,  "-",   1,  5, 0) Path no W, show only artefacts
	--Mandian Droite                                                       
	initArtnetPixelMapping("1156754", '172.16.207.14', 40, 109, 20-1,  "-",   1, 25, 0) Path and show no W
	--Autel Statue Jardin                                               
	initArtnetPixelMapping("1156758", '172.16.207.15', 20,  72, 21-1,  "-",   1,  5, 0) Path and show no W
	--Autel Statue Cours                                                     
	initArtnetPixelMapping("1156759", '172.16.207.16', 20,  72, 22-1,  "-",   1,  5, 0) Path and show no W

end

--Starting Player
logicServer("Mapping Led to fixture")
Player.Startup:addStartupCallback("customPlayerStartup", myStartupCallback() )
