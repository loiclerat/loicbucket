## This script will:
## - replace the XX-init.lua and experience.xml files in the X-Agora project by a safe copy in case they are corrupted
## - archive snapshots.zip in case it's corrupted and to avoid it growwing too big
## - Delete all log files older that 200 days

# These solders should be at the root of the X-Agora project folder
$safeFilesLocation = ".\safeFiles\"
$snapshotsArchiveLocation = ".\snapshotsArchive\"
$xagoraLogsLocation = "C:\X-Agora\xframe-server\logs\"

# Test snapshotsArchive path and create if necessary
if (-not (Test-Path -Path $snapshotsArchiveLocation))
{
	New-Item -ItemType "directory" -Path $snapshotsArchiveLocation
}

$snapshotsFile = "snapshots.zip" 
$date = Get-Date -Format "yyyMMdd"
$archivedFileDestination = $snapshotsArchiveLocation + $date + "_" + $snapshotsFile

# Archive snapshots.zip
Move-Item -Path $snapshotsFile -Destination $archivedFileDestination


# Check if safe files copy available
if (Test-Path -Path $safeFilesLocation)
{
	cd $safeFilesLocation
	
	# Replace files with safe copy 
	Copy-Item -Path "experience.xml","player-init.lua","server-init.lua","designer-init.lua" -Destination "..\"
}

# Delete all log files older that 200 days
$dateToDelete = (Get-Date).AddDays(-200)
# Do it recursively so that we delete logs, commands and "saved" logs
Get-ChildItem $xagoraLogsLocation -Recurse | Where-Object { $_.LastWriteTime -lt $DatetoDelete } | Remove-Item -Recurse -Force -Confirm:$false
