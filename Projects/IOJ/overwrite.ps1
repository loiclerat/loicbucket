## This script will:
## - stop the Server and the Player
## - replace the files in the X-Agora project by a safe copy in the specified folder
## - restart the Server (the Player will restart automatically)

## It takes 2 arguments: first is the path of the folder containing the safe files, second is the project name
function process($safeFilesLocation, $projectName)
{	
	"_________________________________"
	Get-Date
	"Executing overwrite script...`n"
	
	if ([string]::IsNullOrWhitespace($safeFilesLocation) -or [string]::IsNullOrWhitespace($projectName))
	{
		"Invalid parameters."
		return
	}
	

	## Stop the Server

	"`nStopping X-Agora Server..."

	sc.exe stop "XagoraServer2.8" 						# Stop the Server service
	(Get-Service "XagoraServer2.8").WaitForStatus('Stopped')		# Wait until the service is properly stopped


	## Stop any Player running

	"`nStopping X-Agora Player..."

	$xframeProcess = Get-Process -Name "xframe-3d" -ErrorAction SilentlyContinue
	if ($xframeProcess)
	{
		$xframeProcess.CloseMainWindow() | Out-Null		# Gracefully stop the Player if it's running	
		
		$xframeProcess | Wait-Process -Timeout 8	# Give 5 seconds max for the Player to stop
		if (!$xframeProcess.HasExited) 
		{
			# If it has not stopped after 5 seconds, we kill the process
			"`nKilling X-Agora Player"
			
			$xframeProcess | Stop-Process -Force
		}
	}

	$xagoraProjectFolder = Join-Path -Path $env:XAGORA_PROJECTS -ChildPath $projectName
	$undoLocation = Join-Path -Path $safeFilesLocation -ChildPath "undo"

	"Copying files"

	# Just in case, we first create a backup of the files currently present in the X-Agora project folder
	cd $xagoraProjectFolder
	Copy-Item -Path "$projectName.scene.integration.xml","player-init.lua","server-init.lua","designer-init.lua","System.sx" -Destination $undoLocation

	# Copy the safe files to the X-Agora project folder
	Copy-Item -Path "$safeFilesLocation\*" -Destination $xagoraProjectFolder -Exclude "undo"


	## Start the Server

	"Starting X-Agora Server"

	sc.exe start "XagoraServer2.8"
	
	"`nExecution complete."
}

$scriptPath = Split-Path $script:MyInvocation.MyCommand.Path
$logFilePath = "$scriptPath\log.txt"

# Flush the log file if it's too big (more than 10MB)
if((Get-Item $logFilePath).length / 1MB -gt 10)
{
	"Flushing log file..."
	Clear-Content $logFilePath
}

# Execute the script redirecting all output to the log.txt file
process $args[0] $args[1] *>> $logFilePath