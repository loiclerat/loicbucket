## This script should be executed on UNV-X600-001 when UNV-X600-005 is the Main

function process($safeFilesLocation, $projectName)
{	
	"_________________________________"
	Get-Date
	"Executing xagora_pull_scene script...`n"
	
	$mainHostname = "UNV-X600-005"
	$backupIP = "172.17.17.13"
	$mainIP = "172.17.17.17"

	## Stop the Agent

	"Stopping X-Agora Agent..."

	Stop-Process -Name "xagora-agentWatchdog" -Force	# Stop the Agent watchdog process
	Stop-Process -Name "xagora-agent_gui" -Force	# Stop the Agent process


	## Stop the Server

	"`nStopping X-Agora Server..."

	sc.exe stop XagoraServer2.6						# Stop the Server service
	(Get-Service XagoraServer2.6).WaitForStatus('Stopped')		# Wait until the service is properly stopped


	## Stop any Player running

	"`nStopping X-Agora Player..."

	$xframeProcess = Get-Process -Name "xframe-3d" -ErrorAction SilentlyContinue
	if ($xframeProcess)
	{
		$xframeProcess.CloseMainWindow() | Out-Null		# Gracefully stop the Player if it's running	
		
		$xframeProcess | Wait-Process -Timeout 5	# Give 5 seconds max for the Player to stop
		if (!$xframeProcess.HasExited) 
		{
			# If it has not stopped after 5 seconds, we kill the process
			"`nKilling X-Agora Player"
			
			$xframeProcess | Stop-Process -Force
		}
	}

	"Download project file from NAS"
	
	robocopy "\\HPH-CLUSTER\backup\From_$mainHostname" "E:\X-Agora Projects\HPH Harry Potter Hollywood" "HPH Harry Potter Hollywood.scene.integration.xml"


	"Changing the host and selected network adapter of the first Player (Server Copy (1) Copy (1)) to put the Backup IP instead"
	
	$integrationFilePath = "E:\X-Agora Projects\HPH Harry Potter Hollywood\HPH Harry Potter Hollywood.scene.integration.xml"

	[xml]$xmlIntegration = Get-Content -Path $integrationFilePath

	$xmlIntegration.integration.players.player | ForEach-Object {
		
		if ($_.name -eq "Server Copy (1) Copy (1)")
		{
			$_.host = $backupIP
			$_.selectedNetworkInterface = $backupIP
		}
		elseif (($_.name -eq "Player 2 Backup") -or ($_.name -eq "Player 3 Backup") -or ($_.name -eq "Player 4 Backup") -or ($_.name -eq "Server Backup"))
		{
			$_.host = $mainIP
			$_.selectedNetworkInterface = $mainIP
		}
	}

	$xmlIntegration.Save($integrationFilePath)
	
	"Restarting Agent and Server"

	# Restart the Agent process and Server service. The Agent and Player will start automatically
	Start-Process -FilePath "C:\X-Agora\xagora-agent\xagora-agentWatchdog.exe"
	sc.exe start XagoraServer2.6
	
	"`nExecution complete."
}

$scriptPath = Split-Path $script:MyInvocation.MyCommand.Path
$logFilePath = "$scriptPath\xagora_pull_scene_log.txt"

# Flush the log file if it's too big (more than 10MB)
if((Get-Item $logFilePath).length / 1MB -gt 10)
{
	"Flushing log file..."
	Clear-Content $logFilePath
}

# Execute the script redirecting all output to the log.txt file
process $args[0] $args[1] *>> $logFilePath
	