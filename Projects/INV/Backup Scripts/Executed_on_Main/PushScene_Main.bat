:: This script should be executed only on the Main machine. 
:: Pushes the whole X-Agora project to the NAS server. We assume the credentials are already stored in Windows Credential Manager.
:: Files already existing on the Syno will be overwritten if older, skipped otherwise
:: If it fails, it retries only 1 time (default wait time between retries is 30s)

robocopy "D:\X-Agora Projects\LCL" "\\LCL-01-NAS-01\Backup\From_LCL-01-XAG01\LCL" /MIR /r:1