# This script should be executed only on the Backup machine. 
# Pulls the whole X-Agora project from the NAS server (the latest files pushed by the Main server).
# Modifies the project to set the Backup server as the new Master
# Stops and restart the X-Agora Server and Player processes/services

# X-Agora 4.9

function main()
{
	### Parameters
	$mainServerHostname = "BJL-01-XAG01"
	$projectName = "BJL"
	$nasHostname = "BJL-01-NAS00"
	$backupFolder = "\\BJL-01-NAS00\Backup\X-Agora"
	$user = "mofa"
	$password = "BJL_m0f4_2116"
	###
	
	"_________________________________"
	Get-Date
	"Executing PullProject script...`n"

	
	"Connecting to the NAS server ($backupFolder)..."
	
	$remoteProjectFolder = Join-Path -Path $backupFolder -ChildPath "$projectName`_$mainServerHostname"
	
	# Authentication
	net use $remoteProjectFolder /u:$nasHostname\$user $password

	if (Test-Path -Path $remoteProjectFolder)
	{
		## Stop Server
		
		"`nStopping X-Agora Server..."
		
		sc.exe stop XagoraServer 				# Stop the Server (in case the project already exists on this machine)
		(Get-Service XagoraServer).WaitForStatus('Stopped')		# Wait until the server is properly stopped
		
		
		## Stop Player
		
		# In case there's a Player currently running on this machine, we need to stop/restart it 
		$xframeProcess = Get-Process -Name "xframe-3d" -ErrorAction SilentlyContinue
		if ($xframeProcess)
		{
			"`nKilling X-Agora Player"
				
			$xframeProcess | Stop-Process -Force
		}	
		
		
		## Download project
		
		"`nDownloading the X-Agora project from the NAS server, please wait..."
		
		$localProjectFolder = Join-Path -Path $env:XAGORA_PROJECTS -ChildPath "$projectName"

		robocopy $remoteProjectFolder $localProjectFolder /MIR /xd snapshots /r:1
		
		
		## Change "isServer" attribute of Players in integration file

		"`nModifying $projectName project to set Backup as the new Master"
		
		$projectFilePath = Join-Path -Path $localProjectFolder -ChildPath "$projectName.scene.integration.xml"

		[xml]$xmlIntegration = Get-Content -Path $projectFilePath

		$xmlIntegration.integration.projectionStudies.projectionStudy | ForEach-Object {
			
			if ($_.name -eq "Default Study")
			{
				$_.playersV4.player | ForEach-Object {
					
					if ($_.name -eq "Backup")
					{
						$_.isServer = "true"
						
					}
					elseif ($_.name -eq "Player 1")
					{
						$_.isServer = "false"
					}
				}
			}
		}

		$xmlIntegration.Save($projectFilePath)
		
		
		## Start Server

		"`nStarting X-Agora Server"
		sc.exe start XagoraServer 				# Start the server
	}
	else
	{
		Write-Error "Cannot find $remoteProjectFolder, make sure the computer is connected to the network and the NAS server is accessible."
	}
}


$scriptPath = Split-Path $script:MyInvocation.MyCommand.Path
$logFilePath = "$scriptPath\PullProject_Backup_log.txt"

# Flush the log file if it's too big (more than 10MB)
if((Get-Item $logFilePath).length / 1MB -gt 10)
{
	"Flushing log file..."
	Clear-Content $logFilePath
}

# Execute the script redirecting all output to the log.txt file
main *>> $logFilePath