# This script should be executed only on the Backup machine. 
# Pulls the whole X-Agora project from the NAS server. We assume the credentials are already stored in Windows Credential Manager.
# We Disable then re-enable the scheduled task that pushes the scene, just in case it happens while the script is running

Disable-ScheduledTask -TaskName "Xagora_PushScene"

$nasHostname = "LCL-01-NAS-01"
$mainHostname = "LCL-01-XAG01"
$projectName = "LCL"

"Connecting to the NAS server ($nasHostname)..."

if (Test-Path -Path "\\$nasHostname\Backup\From_$mainHostname\$projectName")
{
	## Stop Server

	sc.exe stop XagoraServer 				# Stop the Server (in case the project already exists on this machine)
	"Stopping X-Agora Server..."
	(Get-Service XagoraServer).WaitForStatus('Stopped')		# Wait until the server is properly stopped
	
	
	## Download project
	
	"Downloading scene from the NAS server, please wait..."

	robocopy "\\$nasHostname\Backup\From_$mainHostname\$projectName" "D:\X-Agora Projects\$projectName" /MIR /r:1


	## Start Server
	
	sc.exe start XagoraServer 				# Start the server


	## Stop Player
	
	# In case there's a Player currently running on this machine, we need to stop/restart it 
	$xframeProcess = Get-Process -Name "xframe-3d" -ErrorAction SilentlyContinue
	if ($xframeProcess)
	{
		$xframeProcess.CloseMainWindow() | Out-Null		# Gracefully stop the Player if it's running	
		
		$xframeProcess | Wait-Process -Timeout 5	# Give 5 seconds max for the Player to stop
		if (!$xframeProcess.HasExited) 
		{
			# If it has not stopped after 5 seconds, we kill the process
			"`nKilling X-Agora Player"
			
			$xframeProcess | Stop-Process -Force
		}
	}
}
else
{
	Write-Error "Cannot find '\\$nasHostname\Backup\$mainHostname\$projectName', make sure the computer is connected to the network and the NAS server is accessible."
	pause
}

Enable-ScheduledTask -TaskName "Xagora_PushScene"