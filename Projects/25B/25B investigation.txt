= Investigate Agent config lost 
	- 20 Aout, résolu 21 ou 22 Aout
	- 26 Octobre

= Onsite
	= Laptop check
		- System health check
		- Storage
		- Connect/Close Remote Designer
		- Program stuff in timelines, start players, scan library
		- Check save project crashes
	= XAG
		- Start/stop players
		- Connect disconnect Agents
		- System On/Off
			- Check séquence d'allumage. Delay things would help
		- Bridge connection stability
		- Start sync
= Profiling Server/Designer
			
________________________________________________________________________
Profiling

= Server
	- Project loading
		- initializeMediaFolderOgreResourceGroup takes time. The whole resource group management server-side may not be useful
		- Integration loading  (~3s)
			- Load Timeline is the longest
				- Access media by ID would by to biggest optim
				- Parallel loading ?
	- Project save (~3s)
		- Parallel execution ? Mostly for Timelines
		- Skip unused attributes depending on layer and element type + cleanup more unused attributes
		- Optimize stuff in TinyXML ?
		- Execute XML file save async after dumping integration
	- Scan
		- Biggest improvement = optimize getMediaByPathRecursively() by using the path itself to direct the search
		
= Player
	- Project loading
		- Behaviour lookup to remove lua file from blacklist very time consuming
		- Access media by ID other biggest optim
		- Parallel loading of timelines ?
	- Idle
		- KiNet Manager thread a bit busy
	
= Designer
	- TimeCuesCollectionView.Refresh() and such are pretty long to process
	
		
- Delete more cloned timelines related code
- Lot of time spent in looking for "remove" in lua commands to trigger garbage collect
- Faster object destruction ? (Projectors is pretty long in xframe-3d)
