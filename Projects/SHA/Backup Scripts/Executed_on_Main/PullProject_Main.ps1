# This script should be executed only on the Main machine. 
# Pulls the X-Agora project files from the NAS server (the latest files pushed by the Backup server).
# Modifies the project to set the Main server as the new Master
# Stops and restart the X-Agora Server and Player processes/services

# X-Agora 4.15

function main()
{
	### Parameters
	$backupServerHostname = "SHA-01-XAG02"
	$projectName = "SHA"
	$nasHostname = "SHA-01-NAS00"
	$backupFolder = "\\SHA-01-NAS00\Backup\X-Agora"
	$user = "mofa"
	$password = "SHA_m0f4_9123"
	###
	
	"_________________________________"
	Get-Date
	"Executing PullProject script...`n"

	
	"Connecting to the NAS server ($backupFolder)..."
	
	$remoteProjectFolder = Join-Path -Path $backupFolder -ChildPath "$projectName`_$backupServerHostname"
	
	# Authentication
	net use $remoteProjectFolder /u:$nasHostname\$user $password

	if (Test-Path -Path $remoteProjectFolder)
	{
		## Stop Server
		
		"`nStopping X-Agora Server..."
		
		sc.exe stop XagoraServer 				# Stop the Server (in case the project already exists on this machine)
		(Get-Service XagoraServer).WaitForStatus('Stopped')		# Wait until the server is properly stopped
		
		
		## Stop Player
		
		# In case there's a Player currently running on this machine, we need to stop/restart it 
		$xframeProcess = Get-Process -Name "xframe-3d" -ErrorAction SilentlyContinue
		if ($xframeProcess)
		{
			"`nKilling X-Agora Player"
				
			$xframeProcess | Stop-Process -Force
		}	
		
		
		## Download project
		
		"`nDownloading the X-Agora project from the NAS server, please wait..."
		
		$localProjectFolder = Join-Path -Path $env:XAGORA_PROJECTS -ChildPath "$projectName"

		robocopy $remoteProjectFolder $localProjectFolder /MIR /xd snapshots,medias /r:1
		
		
		## Change "isServer" attribute of Players in integration file

		"`nModifying $projectName project to set Player 1 as the new Master"
		
		$projectFilePath = Join-Path -Path $localProjectFolder -ChildPath "$projectName.scene.integration.xml"

		[xml]$xmlIntegration = Get-Content -Path $projectFilePath

		$xmlIntegration.integration.projectionStudies.projectionStudy | ForEach-Object {
			
			if ($_.name -eq "Default Study")
			{
				$_.playersV4.player | ForEach-Object {
					
					if ($_.name -eq "Backup")
					{
						$_.isServer = "false"
						
					}
					elseif ($_.name -eq "Player 1")
					{
						$_.isServer = "true"
					}
				}
			}
		}

		$xmlIntegration.Save($projectFilePath)
		
		
		## Start Server

		"`nStarting X-Agora Server"
		sc.exe start XagoraServer 				# Start the server
	}
	else
	{
		Write-Error "Cannot find $remoteProjectFolder, make sure the computer is connected to the network and the NAS server is accessible."
	}
}


$scriptPath = Split-Path $script:MyInvocation.MyCommand.Path
$logFilePath = "$scriptPath\PullProject_Main_log.txt"

# Flush the log file if it's too big (more than 10MB)
if((Get-Item $logFilePath).length / 1MB -gt 10)
{
	"Flushing log file..."
	Clear-Content $logFilePath
}

# Execute the script redirecting all output to the log.txt file
main *>> $logFilePath