# This script should be executed only on the Backup machine.It resets the Backup to its default config (xagoraShape loaded and Agent connected to Main).
# X-Agora 4.15

function main()
{
	### Parameters
	$mainServerIP = "172.16.0.101"
	$mainServerHostname = "SHA-01-XAG01"
	###
	
	"_________________________________"
	Get-Date
	"Executing Reset script...`n"


	## Stop the Agent

	"Stopping X-Agora Agent..."

	sc.exe stop XagoraAgentWatchdog					# Stop agent watchodg service
	(Get-Service XagoraAgentWatchdog).WaitForStatus('Stopped')		# Wait until the service is properly stopped
	Stop-Process -Name "xagora-agent_gui" -Force	# Stop the Agent process


	## Stop the Server

	"`nStopping X-Agora Server..."

	sc.exe stop XagoraServer 						# Stop the Server service
	(Get-Service XagoraServer).WaitForStatus('Stopped')		# Wait until the service is properly stopped


	## Stop any Player running

	"`nStopping X-Agora Player..."

	$xframeProcess = Get-Process -Name "xframe-3d" -ErrorAction SilentlyContinue
	if ($xframeProcess)
	{
		"`nKilling X-Agora Player"
			
		$xframeProcess | Stop-Process -Force
	}


	## Set startup project

	"`nSetting xagoraShape as startup project"

	$serverConfigPath = Join-Path -Path $env:XAGORA_LOCATION -ChildPath "xframe-server\server-config.xml"

	[xml]$xmlServerConfig = Get-Content -Path $serverConfigPath

	$xmlServerConfig.'xagora-server'.server.fallbackInterface = $mainServerIP
	$xmlServerConfig.'xagora-server'.scene.active = "xagoraShape"
	$xmlServerConfig.Save($serverConfigPath)


	## Set Agent config

	$agentConfigPath = Join-Path -Path $env:XAGORA_LOCATION -ChildPath "xagora-agent\daemon-settings.xml"

	[xml]$xmlAgentConfig = Get-Content -Path $agentConfigPath

	$xmlAgentConfig.'daemon-settings'.'connection-config'.ip = $mainServerIP
	$xmlAgentConfig.'daemon-settings'.'connection-config'.hostnames = $mainServerHostname
	$xmlAgentConfig.'daemon-settings'.share.serverIp = $mainServerIP

	$xmlAgentConfig.Save($agentConfigPath)


	## Start the Server

	"Starting X-Agora Server"

	sc.exe start XagoraServer


	## Start the Agent

	"`nStarting X-Agora Agent"

	sc.exe start XagoraAgentWatchdog


	"`nConfiguration complete!"
}


$scriptPath = Split-Path $script:MyInvocation.MyCommand.Path
$logFilePath = "$scriptPath\Reset_Backup_log.txt"

# Flush the log file if it's too big (more than 10MB)
if((Get-Item $logFilePath).length / 1MB -gt 10)
{
	"Flushing log file..."
	Clear-Content $logFilePath
}

# Execute the script redirecting all output to the log.txt file
main *>> $logFilePath