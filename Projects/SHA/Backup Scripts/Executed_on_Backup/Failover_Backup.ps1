# This script should be executed only on the Backup machine.It configures the Backup as the new Master.
# X-Agora 4.15


function main()
{
	### Parameters
	$projectName = "SHA"
	$backupServerIP = "172.16.0.102"
	$backupServerHostname = "SHA-01-XAG02"
	###
	
	"_________________________________"
	Get-Date
	"Executing Failover script...`n"


	## Stop the Agent

	"Stopping X-Agora Agent..."

	sc.exe stop XagoraAgentWatchdog					# Stop agent watchodg service
	(Get-Service XagoraAgentWatchdog).WaitForStatus('Stopped')		# Wait until the service is properly stopped
	Stop-Process -Name "xagora-agent_gui" -Force	# Stop the Agent process


	## Stop the Server

	"`nStopping X-Agora Server..."

	sc.exe stop XagoraServer 						# Stop the Server service
	(Get-Service XagoraServer).WaitForStatus('Stopped')		# Wait until the service is properly stopped


	## Stop any Player running (couldn't find a way to do a clean close)

	"`nKilling X-Agora Player..."

	$xframeProcess = Get-Process -Name "xframe-3d" -ErrorAction SilentlyContinue
	if ($xframeProcess)
	{	
		$xframeProcess | Stop-Process -Force
	}


	## Change "isServer" attribute of Players in integration file

	"`nModifying $projectName project to set Backup as the new Master"
	
	$projectFilePath = Join-Path -Path $env:XAGORA_PROJECTS -ChildPath "$projectName\$projectName.scene.integration.xml"

	[xml]$xmlIntegration = Get-Content -Path $projectFilePath

	$xmlIntegration.integration.projectionStudies.projectionStudy | ForEach-Object {
		
		if ($_.name -eq "Default Study")
		{
			$_.playersV4.player | ForEach-Object {
				
				if ($_.name -eq "Backup")
				{
					$_.isServer = "true"
					
				}
				elseif ($_.name -eq "Player 1")
				{
					$_.isServer = "false"
				}
			}
		}
	}

	$xmlIntegration.Save($projectFilePath)


	## Set startup project

	"Setting $projectName as startup project"
	
	$serverConfigPath = Join-Path -Path $env:XAGORA_LOCATION -ChildPath "xframe-server\server-config.xml"

	[xml]$xmlServerConfig = Get-Content -Path $serverConfigPath

	$xmlServerConfig.'xagora-server'.server.fallbackInterface = $backupServerIP
	$xmlServerConfig.'xagora-server'.scene.active = $projectName
	$xmlServerConfig.Save($serverConfigPath)


	## Set Agent config

	"Changing X-Agora Agent config to connect locally"
	
	$agentConfigPath = Join-Path -Path $env:XAGORA_LOCATION -ChildPath "xagora-agent\daemon-settings.xml"

	[xml]$xmlAgentConfig = Get-Content -Path $agentConfigPath

	$xmlAgentConfig.'daemon-settings'.'connection-config'.ip = $backupServerIP
	$xmlAgentConfig.'daemon-settings'.'connection-config'.hostnames = $backupServerHostname
	$xmlAgentConfig.'daemon-settings'.share.serverIp = $backupServerIP

	$xmlAgentConfig.Save($agentConfigPath)


	## Start the Server

	"Starting X-Agora Server"

	sc.exe start XagoraServer


	## Start the Agent

	"`nStarting X-Agora Agent"

	sc.exe start XagoraAgentWatchdog


	"`nConfiguration complete!"
}


$scriptPath = Split-Path $script:MyInvocation.MyCommand.Path
$logFilePath = "$scriptPath\Failover_Backup_log.txt"

# Flush the log file if it's too big (more than 10MB)
if((Get-Item $logFilePath).length / 1MB -gt 10)
{
	"Flushing log file..."
	Clear-Content $logFilePath
}

# Execute the script redirecting all output to the log.txt file
main *>> $logFilePath
