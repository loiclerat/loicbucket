= NOW	

= LATER
	- Wrap-up
		- Backups
	
= WAITING
______________________________________
Notes:

= Projection
	- Z01: simple keystone
	- Z02: simple keystone
	- Z03: simple keystone. Overshoot top. Trees shadow?
	- Z04: simple keystone
	- Z05: PRJ01 overshoot bottom. PRJ02 1080p instead of UHD
	- Z06: simple keystone
	- Z07: simple keystone
	
= DMX	
	- Universes (base 1)
		- Z00: 1
		- Z01: 1,2
		- Z02: 3,4
		- Z03: 5,6,7,8
		- Z04: 8,9,10
		- Z05: 10,11
		- Z06: 12
		- Z07: 13,14
		
	- TP
		0-5s: Black
		5s-10s: Rouge only
		10s-15s: Vert only
		15s-20s: Bleu only
		20s-25s: White only (if applicable)
		25s-30s: RGBW full
		30s-35s: Black (2)
		35s-40s: Specials (blisslight, dimmers, lanterns -- if applicable to the zone)
		40s-60s: Black (3)
			
______________________________________
