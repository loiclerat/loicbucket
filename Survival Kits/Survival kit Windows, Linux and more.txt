= Tight VNC
	- Only one display: TightVNC Service Configuration -> Extra Ports. Define new port with something like 1920x1080+0+0

= Diskpart
	- Run cmd as admin, then command diskpart
	- list disk / list partition
	- select disk # / select partition #
	- delete partition override
	
= Windows activation
	- Generic Windows key : VK7JG-NPHTM-C97JM-9MPGT-3V66T (can fix wrong Edition selected)
	- slmgr /xpr : shows activation status
	
= Pi, network config with VLANs: see https://momentfactory.atlassian.net/wiki/spaces/XAG/pages/1371734388/Configure+VLANs+on+RaspberryPi
	