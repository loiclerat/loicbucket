// This script generates a 1:1 UV remap image in a 16bpc PNG file
// It can be modified to create custom UV remap images

// ==================================================

// Required modules
var fs = require('fs');
var PNG = require("pngjs").PNG;

function map(value, in_min, in_max, out_min, out_max) 
{
	// X-Agora requires 0.5 to be added to the u/v coordinate
    return (value + 0.5 - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


// IMAGE PARAMETERS	
var width = 4160;
var height = 3200;
var channelBitDepth = 16;
var maxChannelValue = Math.pow(2, channelBitDepth) - 1;


function fillBuffer(buffer, inX, inY, outX, outY, cropWidth, cropHeight)
{
	// Fill buffer with colors values
	for (var y = 0; y < cropHeight; ++y) 
	{
		for (var x = 0; x < cropWidth; ++x) 
		{
			var index = ((y + outY) * width + x + outX) * 4;
			
			bitmap[index] = Math.round(map(inX + x, 0, width, 0, maxChannelValue)); // red 
			bitmap[index + 1] = Math.round(map(inY + y, 0, height, 0, maxChannelValue)); // green
			bitmap[index + 2] = 0;
			bitmap[index + 3] = maxChannelValue; // alpha
		}
	}
}


var buffer = new Buffer.alloc(channelBitDepth / 8 * width * height * 4); 	// 4 channels (RGBA)
var bitmap = new Uint16Array(buffer.buffer);

fillBuffer(buffer, 3840, 0, 0, 0, 320, 2160);
fillBuffer(buffer, 0, 2160, 320, 0, 3520, 1040);
fillBuffer(buffer, 3520, 2160, 320, 1040, 640, 1040);
	
// Create PNG file
var png = new PNG({
            width: width,
            height: height,
            bitDepth: channelBitDepth,
            colorType: 6,				// 6 = color with alpha
            inputColorType: 6,			// 6 = color with alpha
            inputHasAlpha: true
        });

png.data = buffer;
png.pack().pipe(fs.createWriteStream('remap.png'));