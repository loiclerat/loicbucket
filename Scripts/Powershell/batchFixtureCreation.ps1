Process {
	# Setup connection 
	$IP = [System.Net.Dns]::GetHostAddresses("127.0.0.1") 
	$Address = [System.Net.IPAddress]::Parse($IP) 
	$Socket = New-Object System.Net.Sockets.TCPClient($Address,5000) 
	$AuthMessage = "authenticate///luasender///luasender///mofa`0"

	# Setup stream wrtier 
	$Stream = $Socket.GetStream() 
	$Writer = New-Object System.IO.StreamWriter($Stream)

	# Write message to stream
	$AuthMessage | % {
		$Writer.Write($_)
		$Writer.Flush()
	}
	
	$nbPixelsPerFixture = 30
	$nbFixtures = 150
	$projectorID = 4
	$hRes = 1920
	$vRes = 1080
	$i = 0;
	$j = 0;
	
	for ($j = 0 ; $j -lt $nbFixtures ; $j++)
	{
		# Set universe
		$Message = "luascript_model///luasender///Model:beginOperation('SetDefaultUniverse')`n"
			$Message = $Message + "Integration:getDisplay({0}):getPixelMapping(0):setDefaultUniverse({1})`n" -f $projectorID,$j
			$Message = $Message + "Model:endOperation('SetDefaultUniverse')"
			
		$Message = $Message + "`0"
		# Write message to stream
		$Message | % {
			$Writer.Write($_)
			$Writer.Flush()
		}
		
			
		# Set channel
		$Message = "luascript_model///luasender///Model:beginOperation('SetDefaultStartChannel')`n"
		$Message = $Message + "Integration:getDisplay({0}):getPixelMapping(0):setDefaultStartChannel(1)`n" -f $projectorID
		$Message = $Message + "Model:endOperation('SetDefaultStartChannel')"
			
		$Message = $Message + "`0"
		# Write message to stream
		$Message | % {
			$Writer.Write($_)
			$Writer.Flush()
		}
		
		$x = [math]::Round($j/$nbFixtures* $hRes, 0) 
		$coordinates = ""
		
		# Create fixtures
		for ($i = 0; $i -lt $nbPixelsPerFixture ; $i++)
		{
			$y = [math]::Round($i/$nbPixelsPerFixture * $vRes, 0)
			$coordinates = $coordinates + ",{0}:{1}" -f $x,$y
		}
		
		$Message = "luascript_model_request///luasender///Model:beginRequest('AddDMXFixture')`n"
		$Message = $Message + "Integration:getDisplay({0}):getPixelMapping(0):requestAddDMXFixture('{1}')`n" -f $projectorID,$coordinates.Substring(1)
		$Message = $Message + "Model:endRequest('AddDMXFixture')"

		$Message = $Message + "`0"
		# Write message to stream
		$Message | % {
			$Writer.Write($_)
			$Writer.Flush()
		}
		
		Start-Sleep -Milliseconds 40
	}
	
	# Close connection and stream
	$Stream.Close()
	$Socket.Close()
}