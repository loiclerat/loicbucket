
Get-ChildItem ".\" -Filter *.edid | 
Foreach-Object {
	$name = $_.Name
	$data = Get-Content $_.FullName

	$lineIndex = 2
	$newData = ""

	while (++$lineIndex -lt $data.length)
	{
		$line = $data[$lineIndex]
		$length = $line.length
		$newData += $line.substring(8, ($length - 8))
		$newData += '  '
	}

	$newData = $newData -replace '  ', ' '
	$length = $newData.length
	$newData = $newData.substring(0, ($length - 2))
	
	$path = ".\Processed\"
	$path = $path + $name
	
	Out-File -NoNewLine -encoding UTF8 $path -InputObject $newData
}