#Make sure Set-ExecutionPolicy Unrestricted is set (in powershell) on the machine.
#The physical interface has to have an active connection (cable connected) for this script to work.

#Name of the vSwitch on which the vlans should be created. If it already exists, it can be found in Hyper-V Manager.
$switchName = "Ki-Net-vSwitch"

#IP suffix of the interfaces : 253 for main machine, 252 for backup, 251 for operation PC
$suffix = "252"

#Check if the vSwitch already exists -- If not, create it
$switchExists = Get-VMSwitch | Where-Object { $_.Name -eq $switchName }

if ($switchExists) {
    Write-Output "VM switch '$switchName' already exists"
} else {
    Write-Output "VM switch '$switchName' does not exist, creating it..."

    New-VMSwitch -name $switchName -NetAdapterName Ki-Net -AllowManagementOS $true
    Remove-VMNetworkAdapter -ManagementOS -Name $switchName
}

Start-Sleep -s 5

#Remove all vlans from all interfaces -- to remove all vlans created with this script, comment every line after these and run the script
$adapters = Get-NetAdapter | Select-Object Name

foreach($adapter in $adapters){
    
    if( $adapter.Name -match "vEthernet" ){

        $parts = $adapter.Name -split '\(|\)'

        if ($parts.Length -gt 1) {
            $textBetweenParentheses = $parts[1].Trim()

            Remove-VMNetworkAdapter -ManagementOS -Name $textBetweenParentheses

            Write-Output "$($adapter.Name) will be removed"
        }
    }
}

#Sleep a bit to let the adapter enable itself
Start-Sleep -s 5

#vlans2.txt contains info about the vlans and IPs. Each line starts with the vlan ID, followed by any number of IP addresses assigned to it.
$vlans = Get-Content "vlansv2.txt"

#Create new vlan interfaces
$index = 0
foreach ($line in $vlans)
{
	$tokens = $line -split " "
	$vlanid = $tokens[0]
	$nbTokens = $tokens.Count
	
	#Create the vlan interface
    try {
            Add-VMNetworkAdapter -ManagementOS -Name "VLAN$vlanid" -SwitchName $switchName -Passthru | Set-VMNetworkAdapterVlan -Access -VlanId $vlanid
            Write-Output "Interface VLAN$vlanid created"
        } catch {
            Write-Output "Could not create interface : $($_.Exception.Message)"
        }	
	
	#First vlan creation may take more time for the adapter to be enabled
	if ($index -eq 0)
	{
		Start-Sleep -s 2
	}
	
	Start-Sleep -s 2

	#Remove all existing IPs on this interface, in case there were any
	Remove-NetIPAddress -InterfaceAlias "vEthernet (VLAN$vlanid)" -Confirm:$false
	
	#Create the IP addresses for the interface
	for ($i = 1;$i -lt $nbTokens; $i++)
	{
        try {
            $ip = $tokens[$i]
		    New-NetIPAddress -InterfaceAlias "vEthernet (VLAN$vlanid)" -IPAddress "$ip.$suffix" -PrefixLength 24 | Out-Null
        } catch {
            Write-Output "Error on iteration $i : $($_.Exception.Message)"
        }		       
	}
	
	$index++
}

Write-Host "Script completed | $index interfaces have been created"