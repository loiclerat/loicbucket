$lines = 0
$files = 0
Get-ChildItem -File -Recurse -force | ForEach-Object {
	if ($_.extension -in ".cs", ".xaml")
	{
		$files++
		echo $_.FullName
		$lines += [int](Get-Content $_.FullName | Measure-Object -line).Lines
	}
}
"Lines: " + $lines
"Files: " + $files
