Process {
	# Setup connection 
	$IP = [System.Net.Dns]::GetHostAddresses("127.0.0.1") 
	$Address = [System.Net.IPAddress]::Parse($IP) 
	$Socket = New-Object System.Net.Sockets.TCPClient($Address,5000) 
	$AuthMessage = "authenticate///luasender///luasender///mofa`0"

	# Setup stream writer 
	$Stream = $Socket.GetStream() 
	$Writer = New-Object System.IO.StreamWriter($Stream)

	# Write message to stream
	$AuthMessage | % {
		$Writer.Write($_)
		$Writer.Flush()
	}
	
	for ($i = 0 ; $i -lt 10 ; $i++)
	{		
		$Message = "luascript_model_request///luasender///Model:beginRequest('AutoPopulate')`n"
		
		$Message += "local timelineName = 'MM2023-B'"
		$Message += "`nlocal rootFolderName = '_03 PROGRAMME B'"
		$Message += "`nlocal spaceMs = 150000"
		$Message += "`nlocal loopStart = 10 * $i"
		$Message += "`nlocal limit = 10"
		$Message += "`nlocal timeline = Integration:getTimelineByName(timelineName)"
		$Message += "`nlocal timelineId = timeline:getID()"
		$Message += "`nlocal mediaLayerId = timeline:getLayerByName('Media Layer 1'):getID()"
		$Message += "`nlocal audioLayerId = timeline:getLayerByName('Audio Track 1'):getID()"
		$Message += "`nlocal rootMedia = Integration:getMediaByNameRecursively(rootFolderName)"
		$Message += "`nlocal audioMediaName = ''"
		$Message += "`nlocal audioMedia = nil"
		$Message += "`nlocal offset = 0"
		$Message += "`nlocal loopCount = 0"
		
		$Message += "`nfor k,media in pairs(rootMedia:getMediasList()) do "
		$Message += "`nif media:getMediaType() == 0 then"
		$Message += "`nif loopCount < limit + loopStart and loopCount >= loopStart then"
		$Message += "`nIntegration:requestCreateMediaElement(timelineId, mediaLayerId, media:getID(), offset, Integration:makeFloat2(0/0, 0/0), 0)"
		$Message += "`nIntegration:requestCreateTimeCue(timelineId, media:getName(), offset)"
		$Message += "`naudioMediaName = string.gsub(media:getName(), '%..*', '.wav')"
		$Message += "`naudioMedia = rootMedia:getMediaByName(audioMediaName)"
		$Message += "`nif audioMedia ~= nil then"
		$Message += "`nIntegration:requestCreateMediaElement(timelineId, audioLayerId, audioMedia:getID(), offset, Integration:makeFloat2(0/0, 0/0), 0)"
		$Message += "`nend"
		$Message += "`nend"
		$Message += "`noffset = offset + spaceMs"
		$Message += "`nloopCount = loopCount + 1"
		$Message += "`nend"
		$Message += "`nend"
		
		$Message += "`nModel:endRequest('AutoPopulate')`n"
			
		$Message += "`0"
		
		# Write message to stream
		$Message | % {
			$Writer.Write($_)
			$Writer.Flush()
		}
		
		# Let the server breathe a bit between commands
		Start-Sleep -Milliseconds 10
	}
	
	# Close connection and stream
	$Stream.Close()
	$Socket.Close()
}