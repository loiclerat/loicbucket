Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "xbox" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Zune" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Bing" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "StickyNotes" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Solitaire" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "YourPhone" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Teams" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Clipchamp" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Outlook" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "ScreenSketch" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Todos" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "HPPCHardwareDiagnosticsWindows" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "HPEasyClean" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "HPPrivacySettings" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "HPDesktopSupportUtilities" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "myHP" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "IntelManagementandSecurityStatus" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "Ink" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-ProvisionedAppxPackage -Online | Where-Object { $_.PackageName -match "HPAudioControl" } | ForEach-Object { Remove-ProvisionedAppxPackage -Online -PackageName $_.PackageName }
Get-AppxPackage | Where-Object { $_.Name -match "xbox"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Bing"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "StickyNotes"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Zune"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Solitaire"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "YourPhone"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "People"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Teams"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Maps"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Office"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Mail"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Clipchamp"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Todos"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "PowerAutomate"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "QuickAssist"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "ScreenSketch"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Cortana"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Outlook"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "HPPCHardwareDiagnosticsWindows"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "HPEasyClean"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "HPPrivacySettings"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "HPDesktopSupportUtilities"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "myHP"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Whiteboard"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "IntelManagementandSecurityStatus"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "Ink"} | Remove-AppxPackage
Get-AppxPackage | Where-Object { $_.Name -match "HPAudioControl"} | Remove-AppxPackage