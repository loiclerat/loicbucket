function Add-MilliSecondsToSystemTime {
    param (
        [int]$milli
    )

    # Get the current system time
    $currentDateTime = Get-Date

    # Add the specified number of milliseconds
    $newDateTime = $currentDateTime.AddMilliseconds($milli)

    # Update the system time
    Set-Date -Date $newDateTime
}

Add-MilliSecondsToSystemTime -milli $args[0]