# This file aims at being placed in a sub-folder of you "Documents" folder, called "WindowsPowerShell" (you can use a junction)
# When you start a powershell session, these functions will be defined and you'll be able to use them
# PowerShell execution policy needs to be set to remote-signed or unrestricted (Set-ExecutionPolicy RemoteSigned)

Set-PSReadlineKeyHandler -Chord Alt+F4 -Function ViExit

# Dot sourcing to get user-defined variables
$variablesPath = Split-Path $script:MyInvocation.MyCommand.Path
$variablesPath += "\variables.ps1"
. $variablesPath

# Dual X-Agora installation (2.8 & 4) - run this script before and after installation of v2.8
function xag2 ()
{
	# Before update
	if (Test-Path "HKLM:\SOFTWARE\X-Agora")
	{
		Rename-Item -Path "HKLM:\SOFTWARE\X-Agora" "XX-Agora"
	}
	# After update
	elseif (Test-Path "HKLM:\SOFTWARE\XX-Agora")
	{
		Rename-Item -Path "HKLM:\SOFTWARE\XX-Agora" "X-Agora"
	}
	else
	{
		echo "X-Agora 4 is not installed"
		Read-Host -Prompt "Press any key to continue or CTRL+C to quit" 
	}
	
	# Kill X-Agora processes
	sc.exe stop XagoraServer2.8
	sc.exe stop XagoraCameraTethering2.8
	Stop-Process -Name "xagora-agentWatchdog" -Force
	Stop-Process -Name "xagora-agent_gui" -Force
	# Delete X-Agora services
	sc.exe delete XagoraCameraTethering2.8
	sc.exe delete XagoraServer2.8
	# Delete agent watchdog scheduled task
	Unregister-ScheduledTask -TaskName "X-Agora Agent Launcher 2.8" -Confirm:$False
}

# Dual X-Agora installation (2.8 & 4) - run this script before and after installation of v4
function xag4 ()
{
	# Before update
	if (Test-Path "HKLM:\SOFTWARE\X-Agora 2.8")
	{
		Rename-Item -Path "HKLM:\SOFTWARE\X-Agora 2.8" "XX-Agora 2.8"
	}
	# After update
	elseif (Test-Path "HKLM:\SOFTWARE\XX-Agora 2.8")
	{
		Rename-Item -Path "HKLM:\SOFTWARE\XX-Agora 2.8" "X-Agora 2.8"
		# Add environment variables for 2.8 that have been removed during update
		[System.Environment]::SetEnvironmentVariable('XAGORA_AGENT_2.8',($xagoraLocation_2 + '\xagora-agent\'),[System.EnvironmentVariableTarget]::Machine)
		[System.Environment]::SetEnvironmentVariable('XAGORA_PLAYER_2.8',($xagoraLocation_2 + '\xagora-projection\'),[System.EnvironmentVariableTarget]::Machine)
	}
	else
	{
		echo "X-Agora 2.8 is not installed"
	}
	
	# Kill X-Agora processes
	sc.exe stop XagoraServer
	sc.exe stop XagoraCameraTethering
	sc.exe stop XagoraAgentWatchdog
	Stop-Process -Name "xagora-agentWatchdog" -Force
	Stop-Process -Name "xagora-agent_gui" -Force
	# Delete X-Agora services
	sc.exe delete XagoraServer
	sc.exe delete XagoraCameraTethering
	sc.exe delete XagoraAgentWatchdog
}

# Reset all config file with default config and load safe scenes
function xareset ()
{
	# Remove existing safe scenes in Project folder
	if (Test-Path ($env:XAGORA_PROJECTS + "\safeScene_4")) { Remove-Item -Path ($env:XAGORA_PROJECTS + "\safeScene_4") -Recurse -Force; }
	if (Test-Path ($env:XAGORA_PROJECTS + "\safeScene_2")) { Remove-Item -Path ($env:XAGORA_PROJECTS + "\safeScene_2") -Recurse -Force; }
	if (Test-Path ($env:XAGORA_PROJECTS + "\safeScene_dev")) { Remove-Item -Path ($env:XAGORA_PROJECTS + "\safeScene_dev") -Recurse -Force; }
	
	# Copy/past and rename safe scenes in project folder
	Copy-Item -Path ($xagoraLocation_4 + "\xframe-data\scenes\baseScene2D") -Destination ($env:XAGORA_PROJECTS + "\safeScene_4") -Recurse -Force
	Copy-Item -Path ($xagoraLocation_2 + "\xframe-data\scenes\baseScene2D") -Destination ($env:XAGORA_PROJECTS + "\safeScene_2") -Recurse -Force
	Copy-Item -Path ($xagoraLocation_dev + "\xagora\xframe-data\common-data\scenes\baseScene2D") -Destination ($env:XAGORA_PROJECTS + "\safeScene_dev") -Recurse -Force
	
	Rename-Item -Path ($env:XAGORA_PROJECTS + "\safeScene_4\basescene2D.scene.integration.xml") -NewName "safeScene_4.scene.integration.xml"
	Rename-Item -Path ($env:XAGORA_PROJECTS + "\safeScene_2\basescene2D.scene.integration.xml") -NewName "safeScene_2.scene.integration.xml"
	Rename-Item -Path ($env:XAGORA_PROJECTS + "\safeScene_dev\basescene2D.scene.integration.xml") -NewName "safeScene_dev.scene.integration.xml"
	
	# Reset server-config.xml (scene, IP, port)
	Set-Content ($xagoraLocation_4 + "\xframe-server\server-config.xml") "<xagora-server>`n`t<server fallbackInterface=`"127.0.0.1`" port=`"5000`" logCompression=`"0`" logDeletion=`"0`" />`n`t<scene active=`"safeScene_4`" />`n`t<slave isSlave=`"false`" masterHost=`"`" masterPort=`"5000`" />`n</xagora-server>"
	Set-Content ($xagoraLocation_2 + "\xframe-server\server-config.xml") "<xagora-server>`n`t<server fallbackInterface=`"127.0.0.1`" port=`"5000`" logCompression=`"0`" logDeletion=`"0`" />`n`t<scene active=`"safeScene_2`" />`n`t<slave isSlave=`"false`" masterHost=`"`" masterPort=`"5000`" />`n</xagora-server>"
	Set-Content ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\server-config.xml") "<xagora-server>`n`t<server fallbackInterface=`"127.0.0.1`" port=`"5000`" logCompression=`"0`" logDeletion=`"0`" />`n`t<scene active=`"safeScene_dev`" />`n`t<slave isSlave=`"false`" masterHost=`"`" masterPort=`"5000`" />`n</xagora-server>"
	Set-Content ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\server-config.xml") "<xagora-server>`n`t<server fallbackInterface=`"127.0.0.1`" port=`"5000`" logCompression=`"0`" logDeletion=`"0`" />`n`t<scene active=`"safeScene_dev`" />`n`t<slave isSlave=`"false`" masterHost=`"`" masterPort=`"5000`" />`n</xagora-server>"
	
	# Reset daemon-settings.xml (IP, port)
	Remove-Item -Path ($xagoraLocation_4 + "\xagora-agent\daemon-settings.xml") -Force
	Remove-Item -Path ($xagoraLocation_2 + "\xagora-agent\daemon-settings.xml") -Force
	Remove-Item -Path ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\daemon-settings.xml") -Force
	Remove-Item -Path ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\daemon-settings.xml") -Force
	Copy-Item -Path ($xagoraLocation_4 + "\xagora-agent\daemon-settings.xml.tmpl") -Destination ($xagoraLocation_4 + "\xagora-agent\daemon-settings.xml") -Force
	Copy-Item -Path ($xagoraLocation_2 + "\xagora-agent\daemon-settings.xml.tmpl") -Destination ($xagoraLocation_2 + "\xagora-agent\daemon-settings.xml") -Force
	Copy-Item -Path ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\daemon-settings.xml.tmpl") -Destination ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\daemon-settings.xml") -Force
	Copy-Item -Path ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\daemon-settings.xml.tmpl") -Destination ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\daemon-settings.xml") -Force
	
	# Reset connection-config.xml (IP, port)
	Set-Content ($xagoraLocation_4 + "\xagora-projection\connection-config.xml") "<?xml version=`"1.0`" encoding=`"UTF-8`" standalone=`"yes`"?>`n<connection-config>`n`t<xframe-server ignore=`"false`" host=`"localhost`" port=`"5000`" />`n`t<authentication login=`"Designer`" password=`"mofa`" master=`"true`" />`n</connection-config>"
	Set-Content ($xagoraLocation_2 + "\xagora-projection\connection-config.xml") "<?xml version=`"1.0`" encoding=`"UTF-8`" standalone=`"yes`"?>`n<connection-config>`n`t<xframe-server ignore=`"false`" host=`"localhost`" port=`"5000`" />`n`t<authentication login=`"Designer`" password=`"mofa`" master=`"true`" />`n</connection-config>"
	Set-Content ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\connection-config.xml") "<?xml version=`"1.0`" encoding=`"UTF-8`" standalone=`"yes`"?>`n<connection-config>`n`t<xframe-server ignore=`"false`" host=`"localhost`" port=`"5000`" />`n`t<authentication login=`"Designer`" password=`"mofa`" master=`"true`" />`n</connection-config>"
	Set-Content ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\connection-config.xml") "<?xml version=`"1.0`" encoding=`"UTF-8`" standalone=`"yes`"?>`n<connection-config>`n`t<xframe-server ignore=`"false`" host=`"localhost`" port=`"5000`" />`n`t<authentication login=`"Designer`" password=`"mofa`" master=`"true`" />`n</connection-config>"
	
	exit
}

# Shortcut function to quickly assign a static IP or put in DHCP the preferred network interface 
function ip
{
	if ($args.count -eq 0)
	{
		netsh interface ip set address $networkInterface dhcp
	}
	elseif ($args.count -eq 1)
	{
		netsh interface ip set address $networkInterface static $args[0]
	}
	elseif ($args.count -eq 2)
	{
		if ($args[1] -like '*255*')
		{
			netsh interface ip set address $networkInterface static 'address='$args[0] 'mask='$args[1]
		}
		else
		{
			netsh interface ip set address $networkInterface static 'address='$args[0] 'gateway='$args[1]
		}
	}
	elseif ($args.count -eq 3)
	{
		netsh interface ip set address $networkInterface static 'address='$args[0] 'mask='$args[1] 'gateway='$args[2]
	}
	
	netsh interface ip show address $networkInterface
}

# Kill every X-Agora process running, of any version
function xakill
{
	sc.exe stop XagoraServer
	sc.exe stop XagoraCameraTethering
	sc.exe stop XagoraAgentWatchdog
	
	Stop-Process -Name "xagora-agentWatchdog" -Force 		2> $null 
	Stop-Process -Name "X-AGORA.Designer" -Force 			2> $null 
	Stop-Process -Name "xagora-mediaEncoder" -Force 		2> $null 
	Stop-Process -Name "X-AGORA.LogViewer" -Force 			2> $null 
	Stop-Process -Name "lua-sender" -Force 					2> $null 
	Stop-Process -Name "XAgora-Profiler" -Force 			2> $null 
	Stop-Process -Name "xframe-3d" -Force 					2> $null 
	Stop-Process -Name "xframe-3d_d" -Force 				2> $null 
	Stop-Process -Name "Xframe-Server_GUI" -Force			2> $null 
	Stop-Process -Name "xframe-server_executable" -Force	2> $null 
	Stop-Process -Name "xagora-agent_gui" -Force			2> $null 
	
	exit
}

function gracefulClose
{
	if ($args.count -ne 1)
	{
		echo "Wrong number of arguments."
		return
	}
	
	$processToKill = Get-Process -Name $args[0] -ErrorAction SilentlyContinue
	if ($processToKill)
	{
		$processToKill.CloseMainWindow()	# Stop the Player if it's running	
	}
}

# Gracefully stop every X-Agora process running
function xastop
{
	gracefulClose("X-AGORA.Designer");
	gracefulClose("xframe-3d");
	gracefulClose("xframe-3d_d");
	gracefulClose("Xframe-Server_GUI");
	gracefulClose("xframe-server_executable");
	gracefulClose("xagora-agent_gui");
	gracefulClose("XAgora-Profiler");
	gracefulClose("X-AGORA.LogViewer");
	gracefulClose("lua-sender");
	gracefulClose("xagora-agentWatchdog");
	gracefulClose("xagora-mediaEncoder");
	
	exit
}

# Create a new project and load it directly in the Designer
# By default creates a 2D project using X-Agora v4 installed with default naming
# Add these arguments : "3d", "dev", "2" ; to specify, respectively, if you want a 3d project, use dev version, use version 2.8
function xanew
{
	if ($args.count -gt 2)
	{
		echo "Too many arguments."
		return
	}
	
	$index = 1
	while (Test-Path ($env:XAGORA_PROJECTS + "\New Project " + ([string]$index).PadLeft(2,'0'))) 
	{ 
		$index++
	}
	
	$is3D = $false
	$version = "4"
	$name = "New Project " + ([string]$index).PadLeft(2,'0')
	
	$i = 0
	while ($i -lt $args.count)
	{
		if ($args[$i] -eq "3d")
		{
			$is3D = $true
		}
		elseif ($args[$i] -eq 2)
		{
			$version = "2"
		}
		elseif ($args[$i] -eq "dev")
		{
			$version = "dev"
		}
		
		$i++
	}
	
	
	
	# Start the Server if not running
	if((-Not (Get-Process "Xframe-Server_GUI" 2> $null )) -and (-Not (Get-Process "xframe-server_executable" 2> $null )))
	{
		if ($version -eq "4") { Start-Process -WorkingDirectory ($xagoraLocation_4 + "\xframe-server\") -FilePath "Xframe-Server_GUI.exe" ; }
		elseif ($version -eq "2") { Start-Process -WorkingDirectory ($xagoraLocation_2 + "\xframe-server\") -FilePath "Xframe-Server_GUI.exe" ; }
		else { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\") -FilePath "Xframe-Server_GUI.exe" ; }
		
		# Give it some time
		Start-Sleep -Milliseconds 500
	}
	
	# Start the Agent if not running
	if(-Not (Get-Process "xagora-agent_gui" 2> $null ))
	{
		$agentDir
		if ($version -eq "4") { $agentDir = $xagoraLocation_4 + "\xagora-agent\" ; }
		elseif ($version -eq "2") { $agentDir = $xagoraLocation_2 + "\xagora-agent\" ; }
		else { $agentDir = $xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\" ; }
		
		Start-Process -WorkingDirectory $agentDir -FilePath ($agentDir + "xagora-agent_gui.exe")
	}
	
	$baseSceneName = ""
	if ($is3D) { $baseSceneName = "baseScene"; } 
	else { $baseSceneName = "baseScene2D"; } 
	
	$baseSceneLocation = ""
	if ($version -eq "4") { $baseSceneLocation = $xagoraLocation_4 + "\xframe-data\scenes\" + $baseSceneName; } 
	elseif ($version -eq "2") { $baseSceneLocation = $xagoraLocation_2 + "\xframe-data\scenes\" + $baseSceneName; } 
	else { $baseSceneLocation = $xagoraLocation_dev + "\xagora\xframe-data\common-data\scenes\" + $baseSceneName; } 
	
	# Create the scene by copy/paste of baseScene
	Copy-Item -Path ($baseSceneLocation) -Destination ($env:XAGORA_PROJECTS + "\" + $name) -Recurse -Force
	Rename-Item -Path ($env:XAGORA_PROJECTS + "\" + $name + "\" + $baseSceneName + ".scene.integration.xml") -NewName ($name + ".scene.integration.xml")
	
	# Start Designer
	if ($version -eq "4") { Start-Process -WorkingDirectory ($xagoraLocation_4 + "\xagora-projection\") -FilePath "X-AGORA.Designer.exe" -ArgumentList $name; } 
	elseif ($version -eq "2") { Start-Process -WorkingDirectory ($xagoraLocation_2 + "\xagora-projection\") -FilePath "X-AGORA.Designer.exe" -ArgumentList $name; } 
	else { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\") -FilePath "X-AGORA.Designer.exe" -ArgumentList $name; } 
}

# Launch dev Agent, Server and Designer (debug)
function xadev
{
	if ($args.count -gt 2)
	{
		echo "Too many arguments."
		return
	}
	
	$version = ""
	$name = ""
	
	$i = 0
	while ($i -lt $args.count)
	{
		if ($args[$i] -eq "debug")
		{
			$version = "debug"
		}
		elseif ($args[$i] -eq "release")
		{
			$version = "release"
		}
		elseif ($i -eq 1)
		{
			$name = $args[$i]
		}
		else
		{
			echo "Wrong arguments."
			return
		}	
			
		$i++
	}
	
	# Start the Server if not running
	if((-Not (Get-Process "Xframe-Server_GUI" 2> $null )) -and (-Not (Get-Process "xframe-server_executable" 2> $null )))
	{
		if ($version -eq "debug") { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\") -FilePath "Xframe-Server_GUI.exe"; }
		elseif ($version -eq "release") { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\") -FilePath "Xframe-Server_GUI.exe"; }
		else { return; }
		
		# Give it some time
		Start-Sleep -Milliseconds 500
	}
	
	# Start the Agent if not running
	if(-Not (Get-Process "xagora-agent_gui" 2> $null ))
	{
		$agentDir
		
		if ($version -eq "debug") { $agentDir = $xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\" ; }
		elseif ($version -eq "release") { $agentDir = $xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\" ; }
		else { return; }
		
		Start-Process -WorkingDirectory $agentDir -FilePath ($agentDir + "xagora-agent_gui.exe")
	}
	
	if ($name)
	{
		# Start Designer
		if ($version -eq "debug") { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\") -FilePath "X-AGORA.Designer.exe" -ArgumentList $name; } 
		elseif ($version -eq "release") { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\") -FilePath "X-AGORA.Designer.exe" -ArgumentList $name; } 
		else { return; } 
	}
	else
	{
		# Start Designer
		if ($version -eq "debug") { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Debug\xagora-xtreme\") -FilePath "X-AGORA.Designer.exe";} 
		elseif ($version -eq "release") { Start-Process -WorkingDirectory ($xagoraLocation_dev + "\xagora_build\Release\xagora-xtreme\") -FilePath "X-AGORA.Designer.exe"; } 
		else { return; } 
	}
	
	exit
}

function debug
{
	if ($args.count -gt 1)
	{
		echo "Too many arguments."
		return
	}
	
	xadev "debug" $args[0]
}

function release
{
	if ($args.count -gt 1)
	{
		echo "Too many arguments."
		return
	}
	
	xadev "release" $args[0]
}

function linkd
{
	Start-Process -WorkingDirectory $xagoraLocation_dev -FilePath "make_links_debug.bat"
	exit
}

function linkr
{
	Start-Process -WorkingDirectory $xagoraLocation_dev -FilePath "make_links_release.bat"
	exit
}

# Offset XAML Grid Row index for all elements between two lines
function rows
{
	Param ( 
		[Parameter(Mandatory=$true, Position=1)]
			[string]
			$FileAbsolutePath
		,
		[Parameter(Mandatory=$true, Position=0)]
			[ValidateNotNullOrEmpty()] 
			[int] 
			$FirstLine
		, 
		[Parameter(Mandatory=$true, Position=0)]
			[ValidateNotNullOrEmpty()] 
			[int] 
			$LastLine
		, 
		[Parameter(Mandatory=$true, Position=0)]
			[ValidateNotNullOrEmpty()] 
			[int] 
			$FirstRowIndexToOffset
		, 
		[Parameter(Mandatory=$true, Position=0)]
			[ValidateNotNullOrEmpty()] 
			[int] 
			$LastRowIndexToOffset
		, 
		[Parameter(Mandatory=$true, Position=0)]
			[ValidateNotNullOrEmpty()] 
			[int] 
			$Increment
	) 

	Process {

		$FileAbsolutePath = $FileAbsolutePath.Replace("`"","")
		$originalContent = Get-Content -Path $FileAbsolutePath
		
		$FirstLine = [int]([math]::Max($FirstLine, 1) - 1)
		$LastLine = [int]([math]::Min($LastLine, $originalContent.length) - 1)
		$FirstRowIndexToOffset = [math]::Min($FirstRowIndexToOffset, 0)
		$LastRowIndexToOffset = [math]::Max($LastRowIndexToOffset, $FirstRowIndexToOffset) 
		
		$subset = $originalContent[$FirstLine..$LastLine]

		if ($Increment -gt 0)
		{
			$index = $LastRowIndexToOffset
			while ($index -gt $FirstRowIndexToOffset - 1)
			{
				$indexOffset = $index + $Increment
				$subset = $subset.replace("Grid.Row=`"$index`"", "Grid.Row=`"$indexOffset`"")
				$index--
			}
		}
		else
		{
			$index = $FirstRowIndexToOffset
			while ($index -lt $LastRowIndexToOffset + 1)
			{
				$indexOffset = $index + $Increment
				$subset = $subset.replace("Grid.Row=`"$index`"", "Grid.Row=`"$indexOffset`"")
				$index++
			}
		}
		
		$index = 0
		while ($index -lt $subset.length)
		{
			$originalContent[$index + $FirstLine] = $subset[$index]
			$index++
		}
		
		$originalContent | Set-Content -Path $FileAbsolutePath
		
		pause
	}
}