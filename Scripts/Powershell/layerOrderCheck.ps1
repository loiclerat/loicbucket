# Path to the folder containing XML files
$folderPath = "D:\integrationFiles\"

# Get all XML files in the folder
$xmlFiles = Get-ChildItem -Path $folderPath -Filter *.xml

foreach ($xmlFile in $xmlFiles) {
    Write-Output "Processing file: $($xmlFile.FullName)"

    # Load the XML file
    [xml]$xml = Get-Content -Path $xmlFile.FullName

    # Iterate through each <timeline> node
    foreach ($timeline in $xml.integration.timelines.timeline) {
        $layersNode = $timeline.layers
        $orderAttribute = $layersNode.order

        if ($orderAttribute) {
            # Split the order attribute into an array of layer IDs
            $orderedLayerIds = $orderAttribute -split ','
			
			if ($orderedLayerIds.Length -eq 1)
			{
				continue
			}

            # Get the <layer> nodes and sort them by their sortId attribute
            $sortedLayers = $layersNode.layer | Sort-Object { [int]$_.sortId }

            # Check if the sorted layers match the order attribute
            $isCorrectOrder = $true
            for ($i = 0; $i -lt $orderedLayerIds.Length; $i++) {
                if ($orderedLayerIds[$i] -ne $sortedLayers[$i].id) {
                    $isCorrectOrder = $false
                    break
                }
            }

            if ($isCorrectOrder) {
                #Write-Output "Timeline with layers order '$orderAttribute' is correctly sorted."
            } else {
                Write-Output "Timeline with layers order '$orderAttribute' is NOT correctly sorted."
            }
        } else {
            Write-Output "Timeline does not have an order attribute."
        }
    }
}