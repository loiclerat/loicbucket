# This is a very basic implementation of a Lua sender for X-Agora in powershell
# It can be used to send lua commands automatically via a powershell script for instance
Param ( 
		[Parameter(Mandatory=$true, Position=0)]
		[ValidateNotNullOrEmpty()] 
		[string] 
		$EndPoint
	, 
		[Parameter(Mandatory=$true, Position=1)]
		[int]
		$Port
) 
Process {
	# Setup connection 
	$IP = [System.Net.Dns]::GetHostAddresses($EndPoint) 
	$Address = [System.Net.IPAddress]::Parse($IP) 
	$Socket = New-Object System.Net.Sockets.TCPClient($Address,$Port) 
	$AuthMessage = "authenticate///luasender///luasender///mofa`0"

	# Setup stream wrtier 
	$Stream = $Socket.GetStream() 
	$Writer = New-Object System.IO.StreamWriter($Stream)

	# Write message to stream
	$AuthMessage | % {
		$Writer.Write($_)
		$Writer.Flush()
	}
	
	while(1)
	{		
		$Message = Read-Host -Prompt 'Input your message '
		if ($Message -eq "exit")
		{
			break
		}
		else
		{
			$Message = $Message + "`0"
			# Write message to stream
			$Message | % {
				$Writer.Write($_)
				$Writer.Flush()
			}
		}
	}
	
	# Close connection and stream
	$Stream.Close()
	$Socket.Close()
}