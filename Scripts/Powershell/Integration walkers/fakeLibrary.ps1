$projectFilePath = "D:\X-Agora Projects\25B_TMPL\25B_TMPL.scene.integration.xml"

[xml]$xmlIntegration = Get-Content -Path $projectFilePath

$basePath = "D:\X-Agora Projects\25B_TMPL\medias\"

function scan($currentObject, $currentPath)
{	
	$currentObject.media | foreach-object {
		
		if ($_ -ne $null)
		{
			$mediaName = $_.name
			
			$path = join-path -path $currentpath -childpath $mediaName
			
			$extension = [system.io.path]::getextension($mediaName)
			
			if ($extension -eq "")
			{
				new-item -itemtype "directory" -path $path
				
				scan $_ $path
			}
			if($extension -eq ".pkg")
			{
				new-item -itemtype "directory" -path $path
				
				$cropsPath = Join-Path $path -ChildPath "crops.xml"
				
				Copy-Item -Path "D:\crops.xml" -Destination $cropsPath
			}
		}
	}
}

$xmlIntegration.SelectNodes("//integration") | ForEach-Object {
	scan $_.medias $basePath
}

pause
clear
