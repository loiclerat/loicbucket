$projectFilePath = "E:\XAP\25B_TMPL.scene.integration.xml"

[xml]$xmlIntegration = Get-Content -Path $projectFilePath

$mediaCount = 0
$missingOnMasterCount = 0
$scanFailedCount = 0
$noElementAssociatedCount = 0
$timelineCount = 0

$xmlIntegration.SelectNodes("//media") | ForEach-Object {
	if ($_ -ne $null)
	{
		++$mediaCount
		
		if ($_.isMissingOnMaster -eq "true")
		{
			++$missingOnMasterCount
		}
		
		if ($_.scanFailed -eq "true")
		{
			++$scanFailedCount
		}
		
		if (($_.isVirtual -eq "true") -or (($_.isDirectory -eq "true") -and ($_.isPackage -eq "false")) -or (($_.ParentNode -ne $null) -and ($_.ParentNode.isPackage -eq "true")))
		{
			
		}
		else
		{
			if ($_.timelineElementAssociations -eq "")
			{
				++$noElementAssociatedCount
			}
		}
	}
}

$xmlIntegration.SelectNodes("//timeline") | ForEach-Object {
	++$timelineCount
}

echo "Total media count : $mediaCount"
echo "Missing on Master : $missingOnMasterCount"
echo "Scan Failed : $scanFailedCount"
echo "Unused Media : $noElementAssociatedCount"
echo "Timelines : $timelineCount"

pause
clear
