$projectName = "25B_TMPL"

$projectFilePath = "$projectName.scene.integration.xml"

[xml]$xmlIntegration = Get-Content -Path $projectFilePath

$count = 0

$xmlIntegration.SelectNodes("//integration") | ForEach-Object {
	$_.timelines.timeline | ForEach-Object {
		$tName = $_.name
		echo "Timeline : $tName"
		$_.layers.layer | ForEach-Object {
			$_.element | ForEach-Object {
				if ($_.owner -eq "")
				{
					$_.owner = "1227551"
					
					$eName = $_.name
					echo "`t $eName"
					$count++
				}
			}
		}
	}
}

$xmlIntegration.Save($projectFilePath)

echo "Changed $count elements"
pause
