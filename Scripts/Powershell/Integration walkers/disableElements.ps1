$projectName = "New Project 07"

$projectFilePath = "D:\X-Agora Projects\$projectName\$projectName.scene.integration.xml"

[xml]$xmlIntegration = Get-Content -Path $projectFilePath

$xmlIntegration.SelectNodes("//layer") | ForEach-Object {
	
	if ($_.disabled -eq "true")
	{
		$_.element | ForEach-Object {
			$_.disabled = "true"
		}
	}
}

$xmlIntegration.Save($projectFilePath)
