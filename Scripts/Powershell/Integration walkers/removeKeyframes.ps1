$projectName = "ATG_Lobby"

$projectFilePath = "D:\X-Agora Projects\$projectName\$projectName.scene.integration.xml"

[xml]$xmlIntegration = Get-Content -Path $projectFilePath

$xmlIntegration.SelectNodes("//parameter") | ForEach-Object {
	
	if ($_.tween)
	{
		$nbKeyframes = 0
		
		$_.tween.keyframes.keyframe | ForEach-Object {
			$nbKeyframes++
		}
		
		if ($nbKeyframes -lt 2 -and $_.useTween -eq "false")
		{
			$_.RemoveChild($_.tween)
		}
	}
}

$xmlIntegration.Save($projectFilePath)
