$projectName = "PAR-fresque_v4"

$projectFilePath = "$projectName.scene.integration.xml"

[xml]$xmlIntegration = Get-Content -Path $projectFilePath

$count = 0
$message = ''

$xmlIntegration.SelectNodes("//integration") | ForEach-Object {
	$_.timelines.timeline | ForEach-Object {
		$tName = $_.name
		$message = $message + "`nTimeline : $tName"
		$_.layers.layer | ForEach-Object {
			$_.element | ForEach-Object {
				if ($_.filters -eq $null -and $_.type -eq "audio")
				{		
					$count++
					$eName = $_.name
					$message = $message + "`n`t $eName"
					
					$filters = $xmlIntegration.CreateElement("filters")
					$_.AppendChild($filters)
					
					$filter = $xmlIntegration.CreateElement("filter")
					$filters.AppendChild($filter)
					
					$filter.SetAttribute("type", "user")
					$filter.SetAttribute("value", "")
				}
			}
		}
	}
}

$xmlIntegration.Save($projectFilePath)

pause
clear

echo "$message"
echo "Changed $count elements"

pause
