$adapters = Get-NetAdapter | Select-Object Name

foreach($adapter in $adapters){
    
    if( $adapter.Name -match "vEthernet" ){

        $parts = $adapter.Name -split '\(|\)'

        if ($parts.Length -gt 1) {
            $textBetweenParentheses = $parts[1].Trim()

            Remove-VMNetworkAdapter -ManagementOS -Name $textBetweenParentheses

            Write-Output "$($adapter.Name) removed"
        }

    }

}