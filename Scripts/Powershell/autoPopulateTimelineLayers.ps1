Process {
	# Setup connection 
	$IP = [System.Net.Dns]::GetHostAddresses("127.0.0.1") 
	$Address = [System.Net.IPAddress]::Parse($IP) 
	$Socket = New-Object System.Net.Sockets.TCPClient($Address,5000) 
	$AuthMessage = "authenticate///luasender///luasender///mofa`0"

	# Setup stream writer 
	$Stream = $Socket.GetStream() 
	$Writer = New-Object System.IO.StreamWriter($Stream)

	# Write message to stream
	$AuthMessage | % {
		$Writer.Write($_)
		$Writer.Flush()
	}
	
	# Add media files 'CHANNEL COUNT TEST - X.wav' to audio layers 'Bus 04 - X'
	# X = 1 to 32
	
	for ($i = 1 ; $i -lt 33 ; $i++)
	{		
		$Message = "luascript_model_request///luasender///Model:beginRequest('AddNewMediaElement')`n"
		
		$Message += "local timelineName = 'Timeline 04'`n"
		$Message += "local timeline = Integration:getTimelineByName(timelineName)`n"
		$Message += "local timelineId = timeline:getID()`n"

		$Message += "local mediaName = 'CHANNEL COUNT TEST - ' .. string.format('%02d', $i) .. ' - Copy.wav'`n"
		$Message += "local mediaId = Integration:getMediaByNameRecursively(mediaName):getID()`n"
		$Message += "local layerId = timeline:getLayerByName('Bus 04 - ' .. $i):getID()`n"
		$Message += "Integration:requestCreateMediaElement(timelineId, layerId, mediaId, 0, Integration:makeFloat2(0/0, 0/0), 0)`n"

		$Message += "Model:endRequest('AddNewMediaElement')"
			
		$Message += "`0"
		
		# Write message to stream
		$Message | % {
			$Writer.Write($_)
			$Writer.Flush()
		}
		
		# Let the server breathe a bit between commands
		Start-Sleep -Milliseconds 10
	}
	
	# Close connection and stream
	$Stream.Close()
	$Socket.Close()
}